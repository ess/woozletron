package mock

import (
	"codeberg.org/ess/woozletron/pkg/core"
)

// GranfaloonRepo is a mock implementation of the GranfaloonRepo interface.
type GranfaloonRepo struct {
	CreateFunc            func(granfaloon *core.Granfaloon) error
	GetFunc               func(id string) (*core.Granfaloon, error)
	ByWampeterFunc        func(wampeter *core.Wampeter) []*core.Granfaloon
	ByFomaFunc            func(foma *core.Foma) []*core.Granfaloon
	AllFunc               func() []*core.Granfaloon
	UpdateFunc            func(granfaloon *core.Granfaloon) error
	DeleteFunc            func(granfaloon *core.Granfaloon) error
	ClearFunc             func() error
	CountFunc             func() int
	FomasFunc             func(granfaloon *core.Granfaloon) []*core.Foma
	WampetersFunc         func(granfaloon *core.Granfaloon) []*core.Wampeter
	LinkFomaFunc          func(granfaloon *core.Granfaloon, foma *core.Foma) error
	CreateInvoked         bool
	GetInvoked            bool
	ByWampeterInvoked     bool
	ByFomaInvoked         bool
	AllInvoked            bool
	UpdateInvoked         bool
	DeleteInvoked         bool
	ClearInvoked          bool
	CountInvoked          bool
	FomasInvoked          bool
	WampetersInvoked      bool
	LinkFomaInvoked       bool
	CreateInvocations     int
	GetInvocations        int
	ByWampeterInvocations int
	ByFomaInvocations     int
	AllInvocations        int
	UpdateInvocations     int
	DeleteInvocations     int
	ClearInvocations      int
	CountInvocations      int
	FomasInvocations      int
	WampetersInvocations  int
	LinkFomaInvocations   int
}

// Create is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) Create(granfaloon *core.Granfaloon) error {
	m.CreateInvoked = true
	m.CreateInvocations++
	return m.CreateFunc(granfaloon)
}

// Get is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) Get(id string) (*core.Granfaloon, error) {
	m.GetInvoked = true
	m.GetInvocations++
	return m.GetFunc(id)
}

// ByWampeter is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) ByWampeter(wampeter *core.Wampeter) []*core.Granfaloon {
	m.ByWampeterInvoked = true
	m.ByWampeterInvocations++
	return m.ByWampeterFunc(wampeter)
}

// ByFoma is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) ByFoma(foma *core.Foma) []*core.Granfaloon {
	m.ByFomaInvoked = true
	m.ByFomaInvocations++
	return m.ByFomaFunc(foma)
}

// All is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) All() []*core.Granfaloon {
	m.AllInvoked = true
	m.AllInvocations++
	return m.AllFunc()
}

// Update is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) Update(granfaloon *core.Granfaloon) error {
	m.UpdateInvoked = true
	m.UpdateInvocations++
	return m.UpdateFunc(granfaloon)
}

// Delete is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) Delete(granfaloon *core.Granfaloon) error {
	m.DeleteInvoked = true
	m.DeleteInvocations++
	return m.DeleteFunc(granfaloon)
}

// Clear is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) Clear() error {
	m.ClearInvoked = true
	m.ClearInvocations++
	return m.ClearFunc()
}

// Count is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) Count() int {
	m.CountInvoked = true
	m.CountInvocations++
	return m.CountFunc()
}

// Fomas is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) Fomas(granfaloon *core.Granfaloon) []*core.Foma {
	m.FomasInvoked = true
	m.FomasInvocations++
	return m.FomasFunc(granfaloon)
}

// Wampeters is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) Wampeters(granfaloon *core.Granfaloon) []*core.Wampeter {
	m.WampetersInvoked = true
	m.WampetersInvocations++
	return m.WampetersFunc(granfaloon)
}

// LinkFoma is a mock implementation of the GranfaloonRepo interface.
func (m *GranfaloonRepo) LinkFoma(granfaloon *core.Granfaloon, foma *core.Foma) error {
	m.LinkFomaInvoked = true
	m.LinkFomaInvocations++
	return m.LinkFomaFunc(granfaloon, foma)
}

// NewGranfaloonRepo returns a new mock implementation of the GranfaloonRepo interface.
func NewGranfaloonRepo() *GranfaloonRepo {
	return &GranfaloonRepo{
		CreateFunc: func(granfaloon *core.Granfaloon) error {
			return nil
		},
		GetFunc: func(id string) (*core.Granfaloon, error) {
			return nil, nil
		},
		ByWampeterFunc: func(wampeter *core.Wampeter) []*core.Granfaloon {
			return nil
		},
		ByFomaFunc: func(foma *core.Foma) []*core.Granfaloon {
			return nil
		},
		AllFunc: func() []*core.Granfaloon {
			return nil
		},
		UpdateFunc: func(granfaloon *core.Granfaloon) error {
			return nil
		},
		DeleteFunc: func(granfaloon *core.Granfaloon) error {
			return nil
		},
		ClearFunc: func() error {
			return nil
		},
		CountFunc: func() int {
			return 0
		},
		FomasFunc: func(granfaloon *core.Granfaloon) []*core.Foma {
			return nil
		},
		WampetersFunc: func(granfaloon *core.Granfaloon) []*core.Wampeter {
			return nil
		},
		LinkFomaFunc: func(granfaloon *core.Granfaloon, foma *core.Foma) error {
			return nil
		},
	}
}
