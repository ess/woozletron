package mock

import (
	"codeberg.org/ess/woozletron/pkg/core"
)

// FomaGranfaloonRepo is a mock implementation of the FomaGranfaloonRepo interface.
type FomaGranfaloonRepo struct {
	LinkFunc               func(foma *core.Foma, granfaloon *core.Granfaloon) error
	UnlinkFunc             func(foma *core.Foma, granfaloon *core.Granfaloon) error
	ClearFunc              func() error
	CountFunc              func() int
	FomasFunc              func(granfaloon *core.Granfaloon) []*core.Foma
	GranfaloonsFunc        func(foma *core.Foma) []*core.Granfaloon
	LinkInvoked            bool
	UnlinkInvoked          bool
	ClearInvoked           bool
	CountInvoked           bool
	FomasInvoked           bool
	GranfaloonsInvoked     bool
	LinkInvocations        int
	UnlinkInvocations      int
	ClearInvocations       int
	CountInvocations       int
	FomasInvocations       int
	GranfaloonsInvocations int
}

// Link is a mock implementation of the FomaGranfaloonRepo interface.
func (m *FomaGranfaloonRepo) Link(foma *core.Foma, granfaloon *core.Granfaloon) error {
	m.LinkInvoked = true
	m.LinkInvocations++
	return m.LinkFunc(foma, granfaloon)
}

// Unlink is a mock implementation of the FomaGranfaloonRepo interface.
func (m *FomaGranfaloonRepo) Unlink(foma *core.Foma, granfaloon *core.Granfaloon) error {
	m.UnlinkInvoked = true
	m.UnlinkInvocations++
	return m.UnlinkFunc(foma, granfaloon)
}

// Clear is a mock implementation of the FomaGranfaloonRepo interface.
func (m *FomaGranfaloonRepo) Clear() error {
	m.ClearInvoked = true
	m.ClearInvocations++
	return m.ClearFunc()
}

// Count is a mock implementation of the FomaGranfaloonRepo interface.
func (m *FomaGranfaloonRepo) Count() int {
	m.CountInvoked = true
	m.CountInvocations++
	return m.CountFunc()
}

// Fomas is a mock implementation of the FomaGranfaloonRepo interface.
func (m *FomaGranfaloonRepo) Fomas(granfaloon *core.Granfaloon) []*core.Foma {
	m.FomasInvoked = true
	m.FomasInvocations++
	return m.FomasFunc(granfaloon)
}

// Granfaloons is a mock implementation of the FomaGranfaloonRepo interface.
func (m *FomaGranfaloonRepo) Granfaloons(foma *core.Foma) []*core.Granfaloon {
	m.GranfaloonsInvoked = true
	m.GranfaloonsInvocations++
	return m.GranfaloonsFunc(foma)
}

// NewFomaGranfaloonRepo returns a new mock instance.
func NewFomaGranfaloonRepo() *FomaGranfaloonRepo {
	return &FomaGranfaloonRepo{
		LinkFunc: func(foma *core.Foma, granfaloon *core.Granfaloon) error {
			return nil
		},
		UnlinkFunc: func(foma *core.Foma, granfaloon *core.Granfaloon) error {
			return nil
		},
		ClearFunc: func() error {
			return nil
		},
		CountFunc: func() int {
			return 0
		},
		FomasFunc: func(granfaloon *core.Granfaloon) []*core.Foma {
			return nil
		},
		GranfaloonsFunc: func(foma *core.Foma) []*core.Granfaloon {
			return nil
		},
	}
}
