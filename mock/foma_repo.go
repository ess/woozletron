package mock

import (
	"codeberg.org/ess/woozletron/pkg/core"
)

// FomaRepo is a mock implementation of the FomaRepo interface.
type FomaRepo struct {
	CreateFunc                func(foma *core.Foma, wampeter *core.Wampeter) error
	GetFunc                   func(id string) (*core.Foma, error)
	ByWampeterFunc            func(wampeter *core.Wampeter) []*core.Foma
	ByGranfaloonFunc          func(granfaloon *core.Granfaloon) []*core.Foma
	AllFunc                   func() []*core.Foma
	UpdateFunc                func(foma *core.Foma) error
	DeleteFunc                func(foma *core.Foma) error
	ClearFunc                 func() error
	CountFunc                 func() int
	GranfaloonsFunc           func(foma *core.Foma) []*core.Granfaloon
	LinkGranfaloonFunc        func(foma *core.Foma, granfaloon *core.Granfaloon) error
	CreateInvoked             bool
	GetInvoked                bool
	ByWampeterInvoked         bool
	ByGranfaloonInvoked       bool
	AllInvoked                bool
	UpdateInvoked             bool
	DeleteInvoked             bool
	ClearInvoked              bool
	CountInvoked              bool
	GranfaloonsInvoked        bool
	LinkGranfaloonInvoked     bool
	CreateInvocations         int
	GetInvocations            int
	ByWampeterInvocations     int
	ByGranfaloonInvocations   int
	AllInvocations            int
	UpdateInvocations         int
	DeleteInvocations         int
	ClearInvocations          int
	CountInvocations          int
	GranfaloonsInvocations    int
	LinkGranfaloonInvocations int
}

// Create is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) Create(foma *core.Foma, wampeter *core.Wampeter) error {
	m.CreateInvoked = true
	m.CreateInvocations++
	return m.CreateFunc(foma, wampeter)
}

// Get is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) Get(id string) (*core.Foma, error) {
	m.GetInvoked = true
	m.GetInvocations++
	return m.GetFunc(id)
}

// ByWampeter is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) ByWampeter(wampeter *core.Wampeter) []*core.Foma {
	m.ByWampeterInvoked = true
	m.ByWampeterInvocations++
	return m.ByWampeterFunc(wampeter)
}

// ByGranfaloon is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) ByGranfaloon(granfaloon *core.Granfaloon) []*core.Foma {
	m.ByGranfaloonInvoked = true
	m.ByGranfaloonInvocations++
	return m.ByGranfaloonFunc(granfaloon)
}

// All is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) All() []*core.Foma {
	m.AllInvoked = true
	m.AllInvocations++
	return m.AllFunc()
}

// Update is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) Update(foma *core.Foma) error {
	m.UpdateInvoked = true
	m.UpdateInvocations++
	return m.UpdateFunc(foma)
}

// Delete is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) Delete(foma *core.Foma) error {
	m.DeleteInvoked = true
	m.DeleteInvocations++
	return m.DeleteFunc(foma)
}

// Clear is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) Clear() error {
	m.ClearInvoked = true
	m.ClearInvocations++
	return m.ClearFunc()
}

// Count is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) Count() int {
	m.CountInvoked = true
	m.CountInvocations++
	return m.CountFunc()
}

// Granfaloons is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) Granfaloons(foma *core.Foma) []*core.Granfaloon {
	m.GranfaloonsInvoked = true
	m.GranfaloonsInvocations++
	return m.GranfaloonsFunc(foma)
}

// LinkGranfaloon is a mock implementation of the FomaRepo interface.
func (m *FomaRepo) LinkGranfaloon(foma *core.Foma, granfaloon *core.Granfaloon) error {
	m.LinkGranfaloonInvoked = true
	m.LinkGranfaloonInvocations++
	return m.LinkGranfaloonFunc(foma, granfaloon)
}

// NewFomaRepo returns a new mock FomaRepo.
func NewFomaRepo() *FomaRepo {
	return &FomaRepo{
		CreateFunc:                func(foma *core.Foma, wampeter *core.Wampeter) error { return nil },
		GetFunc:                   func(id string) (*core.Foma, error) { return nil, nil },
		ByWampeterFunc:            func(wampeter *core.Wampeter) []*core.Foma { return nil },
		ByGranfaloonFunc:          func(granfaloon *core.Granfaloon) []*core.Foma { return nil },
		AllFunc:                   func() []*core.Foma { return nil },
		UpdateFunc:                func(foma *core.Foma) error { return nil },
		DeleteFunc:                func(foma *core.Foma) error { return nil },
		ClearFunc:                 func() error { return nil },
		CountFunc:                 func() int { return 0 },
		GranfaloonsFunc:           func(foma *core.Foma) []*core.Granfaloon { return nil },
		LinkGranfaloonFunc:        func(foma *core.Foma, granfaloon *core.Granfaloon) error { return nil },
		CreateInvoked:             false,
		GetInvoked:                false,
		ByWampeterInvoked:         false,
		ByGranfaloonInvoked:       false,
		AllInvoked:                false,
		UpdateInvoked:             false,
		DeleteInvoked:             false,
		ClearInvoked:              false,
		CountInvoked:              false,
		GranfaloonsInvoked:        false,
		LinkGranfaloonInvoked:     false,
		CreateInvocations:         0,
		GetInvocations:            0,
		ByWampeterInvocations:     0,
		ByGranfaloonInvocations:   0,
		AllInvocations:            0,
		UpdateInvocations:         0,
		DeleteInvocations:         0,
		ClearInvocations:          0,
		CountInvocations:          0,
		GranfaloonsInvocations:    0,
		LinkGranfaloonInvocations: 0,
	}
}
