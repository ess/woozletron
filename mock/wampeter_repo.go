package mock

import (
	"codeberg.org/ess/woozletron/pkg/core"
)

// WampeterRepo is a mock implementation of the WampeterRepo interface.
type WampeterRepo struct {
	CreateFunc             func(wampeter *core.Wampeter) error
	GetFunc                func(id string) (*core.Wampeter, error)
	AllFunc                func() []*core.Wampeter
	UpdateFunc             func(wampeter *core.Wampeter) error
	DeleteFunc             func(wampeter *core.Wampeter) error
	ClearFunc              func() error
	CountFunc              func() int
	FomasFunc              func(wampeter *core.Wampeter) []*core.Foma
	GranfaloonsFunc        func(wampeter *core.Wampeter) []*core.Granfaloon
	CreateInvoked          bool
	GetInvoked             bool
	AllInvoked             bool
	UpdateInvoked          bool
	DeleteInvoked          bool
	ClearInvoked           bool
	CountInvoked           bool
	FomasInvoked           bool
	GranfaloonsInvoked     bool
	CreateInvocations      int
	GetInvocations         int
	AllInvocations         int
	UpdateInvocations      int
	DeleteInvocations      int
	ClearInvocations       int
	CountInvocations       int
	FomasInvocations       int
	GranfaloonsInvocations int
}

// Create is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) Create(wampeter *core.Wampeter) error {
	m.CreateInvoked = true
	m.CreateInvocations++
	return m.CreateFunc(wampeter)
}

// Get is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) Get(id string) (*core.Wampeter, error) {
	m.GetInvoked = true
	m.GetInvocations++
	return m.GetFunc(id)
}

// All is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) All() []*core.Wampeter {
	m.AllInvoked = true
	m.AllInvocations++
	return m.AllFunc()
}

// Update is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) Update(wampeter *core.Wampeter) error {
	m.UpdateInvoked = true
	m.UpdateInvocations++
	return m.UpdateFunc(wampeter)
}

// Delete is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) Delete(wampeter *core.Wampeter) error {
	m.DeleteInvoked = true
	m.DeleteInvocations++
	return m.DeleteFunc(wampeter)
}

// Clear is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) Clear() error {
	m.ClearInvoked = true
	m.ClearInvocations++
	return m.ClearFunc()
}

// Count is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) Count() int {
	m.CountInvoked = true
	m.CountInvocations++
	return m.CountFunc()
}

// Fomas is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) Fomas(wampeter *core.Wampeter) []*core.Foma {
	m.FomasInvoked = true
	m.FomasInvocations++
	return m.FomasFunc(wampeter)
}

// Granfaloons is a mock implementation of the WampeterRepo interface.
func (m *WampeterRepo) Granfaloons(wampeter *core.Wampeter) []*core.Granfaloon {
	m.GranfaloonsInvoked = true
	m.GranfaloonsInvocations++
	return m.GranfaloonsFunc(wampeter)
}

// NewWampeterRepo returns a new WampeterRepo.
func NewWampeterRepo() *WampeterRepo {
	return &WampeterRepo{
		CreateFunc:      func(wampeter *core.Wampeter) error { return nil },
		GetFunc:         func(id string) (*core.Wampeter, error) { return nil, nil },
		AllFunc:         func() []*core.Wampeter { return nil },
		UpdateFunc:      func(wampeter *core.Wampeter) error { return nil },
		DeleteFunc:      func(wampeter *core.Wampeter) error { return nil },
		ClearFunc:       func() error { return nil },
		CountFunc:       func() int { return 0 },
		FomasFunc:       func(wampeter *core.Wampeter) []*core.Foma { return nil },
		GranfaloonsFunc: func(wampeter *core.Wampeter) []*core.Granfaloon { return nil },
	}
}
