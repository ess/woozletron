package core

// Foma is a struct that describes a foma.
// It has a string ID.
// It has a name.
// It has a description.
// It has a created timestamp expressed as milliseconds since the epoch.
// It has a modified timestamp expressed as milliseconds since the epoch.
type Foma struct {
	ID          string
	Name        string
	Description string
	Created     int64
	Modified    int64
}
