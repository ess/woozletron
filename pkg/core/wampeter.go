package core

// Wampeter is a struct that describes a wampeter.
// It has a string ID.
// It has a name.
// It has a creation time expressed as milliseconds since the Unix epoch.
type Wampeter struct {
	ID        string
	Name      string
	CreatedAt int64
}
