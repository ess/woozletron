package core

// Granfaloon is a struct that describes a granfaloon.
// It has a string ID.
// It has a name.
// It has a location.
// It has a creation time expressed as milliseconds since the Unix epoch.
type Granfaloon struct {
	ID        string
	Name      string
	Location  string
	CreatedAt int64
}
