package persistence

import (
	"codeberg.org/ess/woozletron/pkg/core"
)

// GranfaloonRepo is an interface that describes the data persistence operations
// that can be performed on granfaloons.
//
// A Granfaloon has and belongs to many Fomas.
// A Granfaloon has and belongs to many Wampeters through Fomas.
//
// The interface specifies the following operations:
// - Create(granfaloon *core.Granfaloon) error
// - Get(id string) (*core.Granfaloon, error)
// - ByWampeter(wampeter *core.Wampeter) []*core.Granfaloon
// - ByFoma(foma *core.Foma) []*core.Granfaloon
// - All() []*core.Granfaloon
// - Update(granfaloon *core.Granfaloon) error
// - Delete(granfaloon *core.Granfaloon) error
// - Clear() error
// - Count() int
// - Fomas(granfaloon *core.Granfaloon) []*core.Foma
// - Wampeters(granfaloon *core.Granfaloon) []*core.Wampeter
// - LinkFoma(granfaloon *core.Granfaloon, foma *core.Foma) error
type GranfaloonRepo interface {
	Create(granfaloon *core.Granfaloon) error
	Get(id string) (*core.Granfaloon, error)
	ByWampeter(wampeter *core.Wampeter) []*core.Granfaloon
	ByFoma(foma *core.Foma) []*core.Granfaloon
	All() []*core.Granfaloon
	Update(granfaloon *core.Granfaloon) error
	Delete(granfaloon *core.Granfaloon) error
	Clear() error
	Count() int
	Fomas(granfaloon *core.Granfaloon) []*core.Foma
	Wampeters(granfaloon *core.Granfaloon) []*core.Wampeter
	LinkFoma(granfaloon *core.Granfaloon, foma *core.Foma) error
}
