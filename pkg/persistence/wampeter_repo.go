package persistence

import (
	"codeberg.org/ess/woozletron/pkg/core"
)

// WampeterRepo is an interface that describes the data persistence operations
// that can be performed on wampeters.
//
// A Wampeter has many Fomas.
// A Wampeter has many Granfaloons through Fomas.
//
// The interface specifies the following operations:
// - Create(wampeter *core.Wampeter) error
// - Get(id string) (*core.Wampeter, error)
// - All() []*core.Wampeter
// - Update(wampeter *core.Wampeter) error
// - Delete(wampeter *core.Wampeter) error
// - Clear() error
// - Count() int
// - Fomas(wampeter *core.Wampeter) []*core.Foma
// - Granfaloons(wampeter *core.Wampeter) []*core.Granfaloon
type WampeterRepo interface {
	Create(wampeter *core.Wampeter) error
	Get(id string) (*core.Wampeter, error)
	All() []*core.Wampeter
	Update(wampeter *core.Wampeter) error
	Delete(wampeter *core.Wampeter) error
	Clear() error
	Count() int
	Fomas(wampeter *core.Wampeter) []*core.Foma
	Granfaloons(wampeter *core.Wampeter) []*core.Granfaloon
}
