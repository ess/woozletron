package persistence

import (
	"testing"

	"codeberg.org/ess/woozletron/mock"
)

func TestSystem(t *testing.T) {
	subject := func() *System {
		return &System{
			Wampeters:   mock.NewWampeterRepo(),
			Fomas:       mock.NewFomaRepo(),
			Granfaloons: mock.NewGranfaloonRepo(),
			Links:       mock.NewFomaGranfaloonRepo(),
		}
	}

	t.Run("NewSystem", func(t *testing.T) {
		system := NewSystem()

		t.Run("it returns a new System", func(t *testing.T) {
			if system == nil {
				t.Error("system is nil")
			}
		})

		t.Run("it returns a System without WampeterRepo", func(t *testing.T) {
			if system.Wampeters != nil {
				t.Error("system.Wampeters != nil")
			}
		})

		t.Run("it returns a System without FomaRepo", func(t *testing.T) {
			if system.Fomas != nil {
				t.Error("system.Fomas != nil")
			}
		})

		t.Run("it returns a System without GranfaloonRepo", func(t *testing.T) {
			if system.Granfaloons != nil {
				t.Error("system.Granfaloons != nil")
			}
		})

		t.Run("it returns a System without FomaGranfaloonRepo", func(t *testing.T) {
			if system.Links != nil {
				t.Error("system.Links != nil")
			}
		})
	})

	t.Run("Clone", func(t *testing.T) {
		system := subject()

		clonedSystem := system.Clone()

		t.Run("it returns a new System", func(t *testing.T) {
			if clonedSystem == nil {
				t.Error("clonedSystem is nil")
			}

			if clonedSystem == system {
				t.Error("clonedSystem == system")
			}
		})

		t.Run("it returns a System with the same WampeterRepo", func(t *testing.T) {
			if clonedSystem.Wampeters != system.Wampeters {
				t.Error("clonedSystem.Wampeters != system.Wampeters")
			}
		})

		t.Run("it returns a System with the same FomaRepo", func(t *testing.T) {
			if clonedSystem.Fomas != system.Fomas {
				t.Error("clonedSystem.Fomas != system.Fomas")
			}
		})

		t.Run("it returns a System with the same GranfaloonRepo", func(t *testing.T) {
			if clonedSystem.Granfaloons != system.Granfaloons {
				t.Error("clonedSystem.Granfaloons != system.Granfaloons")
			}
		})

		t.Run("it returns a System with the same FomaGranfaloonRepo", func(t *testing.T) {
			if clonedSystem.Links != system.Links {
				t.Error("clonedSystem.Links != system.Links")
			}
		})

	})

	t.Run("WithWampeters", func(t *testing.T) {
		system := subject()
		freshWampeters := mock.NewWampeterRepo()

		systemWithWampeters := system.WithWampeters(freshWampeters)

		t.Run("it returns a new System", func(t *testing.T) {
			if systemWithWampeters == nil {
				t.Error("systemWithWampeters is nil")
			}

			if systemWithWampeters == system {
				t.Error("systemWithWampeters == system")
			}
		})

		t.Run("it returns a System with the new WampeterRepo", func(t *testing.T) {
			if systemWithWampeters.Wampeters != freshWampeters {
				t.Error("systemWithWampeters.Wampeters != freshWampeters")
			}

			if systemWithWampeters.Wampeters == system.Wampeters {
				t.Error("systemWithWampeters.Wampeters == system.Wampeters")
			}
		})

		t.Run("it returns a System with the same FomaRepo", func(t *testing.T) {
			if systemWithWampeters.Fomas != system.Fomas {
				t.Error("systemWithWampeters.Fomas != system.Fomas")
			}
		})

		t.Run("it returns a System with the same GranfaloonRepo", func(t *testing.T) {
			if systemWithWampeters.Granfaloons != system.Granfaloons {
				t.Error("systemWithWampeters.Granfaloons != system.Granfaloons")
			}
		})

		t.Run("it returns a System with the same FomaGranfaloonRepo", func(t *testing.T) {
			if systemWithWampeters.Links != system.Links {
				t.Error("systemWithWampeters.Links != system.Links")
			}
		})
	})

	t.Run("WithFomas", func(t *testing.T) {
		system := NewSystem()
		freshFomas := mock.NewFomaRepo()

		systemWithFomas := system.WithFomas(freshFomas)

		t.Run("it returns a new System", func(t *testing.T) {
			if systemWithFomas == nil {
				t.Error("systemWithFomas is nil")
			}

			if systemWithFomas == system {
				t.Error("systemWithFomas == system")
			}
		})

		t.Run("it returns a System with the same WampeterRepo", func(t *testing.T) {
			if systemWithFomas.Wampeters != system.Wampeters {
				t.Error("systemWithFomas.Wampeters != system.Wampeters")
			}
		})

		t.Run("it returns a System with the new FomaRepo", func(t *testing.T) {
			if systemWithFomas.Fomas != freshFomas {
				t.Error("systemWithFomas.Fomas != freshFomas")
			}

			if systemWithFomas.Fomas == system.Fomas {
				t.Error("systemWithFomas.Fomas == system.Fomas")
			}
		})

		t.Run("it returns a System with the same GranfaloonRepo", func(t *testing.T) {
			if systemWithFomas.Granfaloons != system.Granfaloons {
				t.Error("systemWithFomas.Granfaloons != system.Granfaloons")
			}
		})

		t.Run("it returns a System with the same FomaGranfaloonRepo", func(t *testing.T) {
			if systemWithFomas.Links != system.Links {
				t.Error("systemWithFomas.Links != system.Links")
			}
		})
	})

	t.Run("WithGranfaloons", func(t *testing.T) {
		system := subject()
		freshGranfaloons := mock.NewGranfaloonRepo()

		systemWithGranfaloons := system.WithGranfaloons(freshGranfaloons)

		t.Run("it returns a new System", func(t *testing.T) {
			if systemWithGranfaloons == nil {
				t.Error("systemWithGranfaloons is nil")
			}

			if systemWithGranfaloons == system {
				t.Error("systemWithGranfaloons == system")
			}
		})

		t.Run("it returns a System with the same WampeterRepo", func(t *testing.T) {
			if systemWithGranfaloons.Wampeters != system.Wampeters {
				t.Error("systemWithGranfaloons.Wampeters != system.Wampeters")
			}
		})

		t.Run("it returns a System with the same FomaRepo", func(t *testing.T) {
			if systemWithGranfaloons.Fomas != system.Fomas {
				t.Error("systemWithGranfaloons.Fomas != system.Fomas")
			}
		})

		t.Run("it returns a System with the new GranfaloonRepo", func(t *testing.T) {
			if systemWithGranfaloons.Granfaloons != freshGranfaloons {
				t.Error("systemWithGranfaloons.Granfaloons != freshGranfaloons")
			}

			if systemWithGranfaloons.Granfaloons == system.Granfaloons {
				t.Error("systemWithGranfaloons.Granfaloons == system.Granfaloons")
			}
		})

		t.Run("it returns a System with the same FomaGranfaloonRepo", func(t *testing.T) {
			if systemWithGranfaloons.Links != system.Links {
				t.Error("systemWithGranfaloons.Links != system.Links")
			}
		})
	})

	t.Run("WithLinks", func(t *testing.T) {
		system := subject()
		freshLinks := mock.NewFomaGranfaloonRepo()

		systemWithLinks := system.WithLinks(freshLinks)

		t.Run("it returns a new System", func(t *testing.T) {
			if systemWithLinks == nil {
				t.Error("systemWithLinks is nil")
			}

			if systemWithLinks == system {
				t.Error("systemWithLinks == system")
			}
		})

		t.Run("it returns a System with the same WampeterRepo", func(t *testing.T) {
			if systemWithLinks.Wampeters != system.Wampeters {
				t.Error("systemWithLinks.Wampeters != system.Wampeters")
			}
		})

		t.Run("it returns a System with the same FomaRepo", func(t *testing.T) {
			if systemWithLinks.Fomas != system.Fomas {
				t.Error("systemWithLinks.Fomas != system.Fomas")
			}
		})

		t.Run("it returns a System with the same GranfaloonRepo", func(t *testing.T) {
			if systemWithLinks.Granfaloons != system.Granfaloons {
				t.Error("systemWithLinks.Granfaloons != system.Granfaloons")
			}
		})

		t.Run("it returns a System with the new LinkRepo", func(t *testing.T) {
			if systemWithLinks.Links != freshLinks {
				t.Error("systemWithLinks.Links != freshLinks")
			}

			if systemWithLinks.Links == system.Links {
				t.Error("systemWithLinks.Links == system.Links")
			}
		})
	})
}
