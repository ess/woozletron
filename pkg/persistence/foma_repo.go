package persistence

import (
	"codeberg.org/ess/woozletron/pkg/core"
)

// FomaRepo is an interface that describes the data persistence operations
// that can be performed on fomas.
//
// A Foma belongs to a Wampeter.
// A Foma has and belongs to many Granfaloons.
//
// The interface specifies the following operations:
// - Create(foma *core.Foma, wampeter *core.Wampeter) error
// - Get(id string) (*core.Foma, error)
// - ByWampeter(wampeter *core.Wampeter) []*core.Foma
// - ByGranfaloon(granfaloon *core.Granfaloon) []*core.Foma
// - All() []*core.Foma
// - Update(foma *core.Foma) error
// - Delete(foma *core.Foma) error
// - Clear() error
// - Count() int
// - Granfaloons(foma *core.Foma) []*core.Granfaloon
// - LinkGranfaloon(foma *core.Foma, granfaloon *core.Granfaloon) error
type FomaRepo interface {
	Create(foma *core.Foma, wampeter *core.Wampeter) error
	Get(id string) (*core.Foma, error)
	ByWampeter(wampeter *core.Wampeter) []*core.Foma
	ByGranfaloon(granfaloon *core.Granfaloon) []*core.Foma
	All() []*core.Foma
	Update(foma *core.Foma) error
	Delete(foma *core.Foma) error
	Clear() error
	Count() int
	Granfaloons(foma *core.Foma) []*core.Granfaloon
	LinkGranfaloon(foma *core.Foma, granfaloon *core.Granfaloon) error
}
