package persistence

// System is a struct that contains the data persistence layer as a whole.
// It has a WampeterRepo named Wampeters
// It has a FomaRepo named Fomas
// It has a GranfaloonRepo named Granfaloons
// It has a FomaGranfaloonRepo named Links
// It has a function named NewSystem that returns a new System.
// It follows the builder pattern.
// It has a method named Clone that returns a new System based on the current one.
// It has a method named WithWampeters that returns a new System based on the current one with the WampeterRepo replaced.
// It has a method named WithFomas that returns a new System based on the current one with the FomaRepo replaced.
// It has a method named WithGranfaloons that returns a new System based on the current one with the GranfaloonRepo replaced.
// It has a method named WithLinks that returns a new System based on the current one with the FomaGranfaloonRepo replaced.
type System struct {
	Wampeters   WampeterRepo
	Fomas       FomaRepo
	Granfaloons GranfaloonRepo
	Links       FomaGranfaloonRepo
}

// NewSystem is a function that returns a new System.
func NewSystem() *System {
	return &System{}
}

// Clone is a method that returns a new System based on the current one.
func (s *System) Clone() *System {
	return &System{
		Wampeters:   s.Wampeters,
		Fomas:       s.Fomas,
		Granfaloons: s.Granfaloons,
		Links:       s.Links,
	}
}

// WithWampeters is a method that returns a new System based on the current one with the WampeterRepo replaced.
func (s *System) WithWampeters(wampeters WampeterRepo) *System {
	replacement := s.Clone()
	replacement.Wampeters = wampeters

	return replacement
}

// WithFomas is a method that returns a new System based on the current one with the FomaRepo replaced.
func (s *System) WithFomas(fomas FomaRepo) *System {
	replacement := s.Clone()
	replacement.Fomas = fomas

	return replacement
}

// WithGranfaloons is a method that returns a new System based on the current one with the GranfaloonRepo replaced.
func (s *System) WithGranfaloons(granfaloons GranfaloonRepo) *System {
	replacement := s.Clone()
	replacement.Granfaloons = granfaloons

	return replacement
}

// WithLinks is a method that returns a new System based on the current one with the FomaGranfaloonRepo replaced.
func (s *System) WithLinks(links FomaGranfaloonRepo) *System {
	replacement := s.Clone()
	replacement.Links = links

	return replacement
}
