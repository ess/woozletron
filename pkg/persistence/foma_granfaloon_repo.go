package persistence

import "codeberg.org/ess/woozletron/pkg/core"

// FomaGranfaloonRepo is an interface that maps the many-to-many relationship
// between Fomas and Granfaloons.
//
// The interface specifies the following operations:
// - Link(foma *core.Foma, granfaloon *core.Granfaloon) error
// - Unlink(foma *core.Foma, granfaloon *core.Granfaloon) error
// - Clear() error
// - Count() int
// - Fomas(granfaloon *core.Granfaloon) []*core.Foma
// - Granfaloons(foma *core.Foma) []*core.Granfaloon
type FomaGranfaloonRepo interface {
	Link(foma *core.Foma, granfaloon *core.Granfaloon) error
	Unlink(foma *core.Foma, granfaloon *core.Granfaloon) error
	Clear() error
	Count() int
	Fomas(granfaloon *core.Granfaloon) []*core.Foma
	Granfaloons(foma *core.Foma) []*core.Granfaloon
}
